﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Audio; // Needed for SFX
using Microsoft.Xna.Framework.Media; // Needed for music

namespace Button_Masher
{
    /// <summary>
    /// This is the main type for your game.
    /// </summary>
    public class Game1 : Game
    {
        GraphicsDeviceManager graphics;
        SpriteBatch spriteBatch;
        // loaded in game assets 
        Texture2D buttonTexture;
        SpriteFont gameFont;
        SoundEffect clickSFX;
        SoundEffect gameEndSFX;
        Song gameMusic;

        //game state / input
        MouseState previousState;
        int score = 0;
        bool playing = false;
        bool buttonPressed = false;
        float timeRemaining = 0f;
        float timeLimit = 5f;

        public Game1()
        {
            graphics = new GraphicsDeviceManager(this);
            Content.RootDirectory = "Content";
        }

        /// <summary>
        /// Allows the game to perform any initialization it needs to before starting to run.
        /// This is where it can query for any required services and load any non-graphic
        /// related content.  Calling base.Initialize will enumerate through any components
        /// and initialize them as well.
        /// </summary>
        protected override void Initialize()
        {
            // TODO: Add your initialization logic here

            base.Initialize();
        }

        /// <summary>
        /// LoadContent will be called once per game and is the place to load
        /// all of your content.
        /// </summary>
        protected override void LoadContent()
        {
            // Create a new SpriteBatch, which can be used to draw textures.
            spriteBatch = new SpriteBatch(GraphicsDevice);

            // TODO: use this.Content to load your game content here

            //load button graphic
            buttonTexture = Content.Load<Texture2D>("graphics/button");

            //load game font
            gameFont = Content.Load<SpriteFont>("fonts/MainSpriteFont");

            //load SFX
            clickSFX = Content.Load<SoundEffect>("audio/buttonClick");
            gameEndSFX = Content.Load<SoundEffect>("audio/gameOver");
            //load music
            gameMusic = Content.Load<Song>("audio/music");

            //start music
            MediaPlayer.Play(gameMusic);
            MediaPlayer.IsRepeating = true;

            //make it so we can see the mouse 
            IsMouseVisible = true;
        }

        /// <summary>
        /// UnloadContent will be called once per game and is the place to unload
        /// game-specific content.
        /// </summary>
        protected override void UnloadContent()
        {
            // TODO: Unload any non ContentManager content here
        }

        /// <summary>
        /// Allows the game to run logic such as updating the world,
        /// checking for collisions, gathering input, and playing audio.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Update(GameTime gameTime)
        {
            if (GamePad.GetState(PlayerIndex.One).Buttons.Back == ButtonState.Pressed || Keyboard.GetState().IsKeyDown(Keys.Escape))
                Exit();

            // TODO: Add your update logic here

            //get current mouse state 
            MouseState currentState = Mouse.GetState();

            Vector2 screencentre = new Vector2(Window.ClientBounds.Width / 2, Window.ClientBounds.Height / 2);

            //determine button rectangle 
             Rectangle buttonRect = new Rectangle((int)screencentre.X - buttonTexture.Width / 2, (int)screencentre.Y - buttonTexture.Height / 2, buttonTexture.Width, buttonTexture.Height);

            buttonPressed = false;

            //check if we clicked the mouse
            if (currentState.LeftButton == ButtonState.Pressed && previousState.LeftButton != ButtonState.Pressed && buttonRect.Contains(currentState.X, currentState.Y)) 
            {
                buttonPressed = true;
                // mouse is pressed
                clickSFX.Play();
                //add to our score 
                if(playing == true)
                    score = score + 1;
                else // if playing is false
                { // we weren't playing yet and we now should start playing 
                    
                    // set playing to true 
                    playing = true;

                    //set time remaining to the full time limit when we start
                    timeRemaining = timeLimit;

                    //zero out the score for a new game 
                    score = 0;
                }
            }
            if(playing == true)
            {
                // update time remmaining
                //subtract the time passed this frame from our time remaining
                timeRemaining -= (float)gameTime.ElapsedGameTime.TotalSeconds;

                //if we have run out of time , stop the game and clear our time
                if(timeRemaining <= 0)
                {
                    playing = false;
                    timeRemaining = 0;
                    gameEndSFX.Play();
                }

            }

            //current state become previous state 
            previousState = currentState;

            base.Update(gameTime);
        }

        /// <summary>
        /// This is called when the game should draw itself.
        /// </summary>
        /// <param name="gameTime">Provides a snapshot of timing values.</param>
        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.CornflowerBlue);

            // TODO: Add your drawing code here


            //start drawing
            spriteBatch.Begin();

            //find the centre of the screen 
            Vector2 screencentre = new Vector2(Window.ClientBounds.Width / 2, Window.ClientBounds.Height / 2);

            //draw stuff
            if (buttonPressed == false)
            {
                spriteBatch.Draw(buttonTexture, new Rectangle((int)screencentre.X - buttonTexture.Width / 2, (int)screencentre.Y - buttonTexture.Height / 2, buttonTexture.Width, buttonTexture.Height), Color.Green);
            }
            else
            {
                spriteBatch.Draw(buttonTexture, new Rectangle((int)screencentre.X - buttonTexture.Width / 2, (int)screencentre.Y - buttonTexture.Height / 2, buttonTexture.Width, buttonTexture.Height), Color.Yellow);
            }
            //draw text 
            Vector2 titlesize = gameFont.MeasureString("Button Masher");
            Vector2 titlesize2 = gameFont.MeasureString("By Gregor Anderson");
            string promptString = "Click the button to start";
            if (playing == true)
                promptString = " Mash the button";
            Vector2 promptsize = gameFont.MeasureString(promptString);

            spriteBatch.DrawString(gameFont, "Button Masher ", screencentre- new Vector2(0,100)- titlesize/2, Color.White);
            spriteBatch.DrawString(gameFont, "By Gregor Anderson ", screencentre - new Vector2(0, 75) - titlesize2 / 2, Color.White);
            spriteBatch.DrawString(gameFont, promptString, screencentre - new Vector2(0, 50) - promptsize / 2, Color.White);

            spriteBatch.DrawString(gameFont, "Timer:", new Vector2(Window.ClientBounds.Width-150, 10), Color.White);
            spriteBatch.DrawString(gameFont, timeRemaining.ToString(), new Vector2(Window.ClientBounds.Width-50, 10), Color.White);

            spriteBatch.DrawString(gameFont, "Score:", new Vector2(10, 10), Color.White);
            spriteBatch.DrawString(gameFont, score.ToString(), new Vector2(100, 10), Color.White);
            //stop drawing
            spriteBatch.End();

            base.Draw(gameTime);
        }
    }
}
